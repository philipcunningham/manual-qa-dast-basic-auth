require 'pp'

use Rack::Auth::Basic, 'Restricted Area' do |username, password|
  [username, password] == ['hello', 'world']
end

run lambda { |env|
  pp env.select { |key, value| key.include?('HTTP_') }

  [200, { 'Content-Type' => 'text/html' }, ['Hello, world!']]
}
