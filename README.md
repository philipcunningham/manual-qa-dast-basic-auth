# **[DO NOT USE THIS PROJECT](https://about.gitlab.com/handbook/security/#other-servicesdevices)**

> Do not use tools designed to circumvent network firewalls for the purpose of exposing your laptop to the public Internet. An example of this would be using ngrok to generate a public URL for accessing a local development environment. Our core product offers remote code execution as a feature. Other applications we test often expose similar functionality via the relaxed nature of development environments. Running these on a laptop exposed to the Internet would essentially provide a back-door for remote attackers to abuse. This could result in the complete compromise of your home network and all business and personal accounts that have been accessed from your machine. Our Acceptable Use Policy prohibits circumventing the security of any computer owned by GitLab, and using ngrok in this manner is an example of circumventing our documented firewall requirements.

# `manual-qa-dast-basic-auth`

A tiny helper script that starts up and tunnels traffic to a dummy web app using `ngrok`.

## Dependencies

- `ruby`
- `bundler`
- `foreman`
- `ngrok`

### Basic

To build and start the web app run:

```bash
./app setup
./app start
```

### Flow

- Start web app and copy `ngrok` url (https://*.ngrok.io) for DAST on-demand
- Create site profile using URL and [generarted auth header](https://www.debugbear.com/basic-auth-header-generator) `Authorization: Basic aGVsbG86d29ybGQ=`

### Notes

You can also test authentication with `curl` for example:

```
curl --header "Authorization: Basic aGVsbG86d29ybGQ=" {ngrok-url}
```
